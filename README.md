Teste para avaliação Frontend Pleno React
==========================================

Este é o arquivo de requisitos para o teste de candidatos à vaga de desenvolvedor Frontend React Pleno.

Seu objetivo será programar um aplicativo em React que fará um CRUD local para cadastro de produtos, com o controle de acesso de rotas públicas e privadas.

Crie um repositório **público no GitHub ou GitLab** e inicie seu projeto com o create-react-app ou create-next-app.
Depois de finalizado, enviar o link do repositório para o avaliador que entrou em contato.

O projeto **DEVE** ser programado em **Typescript**.

------


Rotas
-----

O projeto contará com as seguintes rotas:

**Rotas Públicas**

* /app/login/
    * Tela de Login
    * Todas as outras URLs serão redirecionadas para essa na parte pública (catch-all das rotas públicas).

**Rotas Privadas**

* /app/cadastrar/
    * Tela de cadastro do produto
* /app/visualizar/
    * Tela de visualização da lista de produtos
* /app/404/
    * Mensagem de erro que indica que o endereço não existe (manter sidebar). Será o endereço catch-all das rotas privadas.

Todas as rotas privadas conterão **uma sidebar** com três botões:

* Visualizar Produtos (link para /app/visualizar/)
* Cadastrar Produtos (link para /app/cadastrar/)
* Logout (apaga o registro do token do localStorage e volta para a área pública)


Controle de autenticação
------------------------

Como não iremos utilizar API para o controle de login, simule um controle de login utilizando o localStorage.

Se houver um registro "token" com qualquer valor, o usuário estará logado. Assim ele pode acessar os endpoints das rotas privadas.

Se não houver o registro "token" no localStorage, o usuário sempre será redirecionado para a área pública (/app/login/).
Depois que o usuário preencher um login e senha, o registro "token" será salvo no localStorage com qualquer valor, e então ele ganhará acesso à parte privada.



-------

Tela de Login
-------------

Programar uma tela de login simples, que conterá um card centralizado com os campos Usuário, Senha e um botão "Acessar".

Faça uma checagem estática para permitir o login caso os campos de usuário e senha seja respectivamente user/123456.

Para qualquer outra combinação, exiba um alerta adequado com a mensagem "Usuário ou senha inválido", em um modal, alerta ou texto dentro do próprio card.


Tela de cadastro do produto
---------------------------

A página terá um componente que será o formulário de criação, que conterá os seguintes campos:

* Código do SKU (int)
* Nome do produto (string)
* Preço (string)
* Categoria (string) -- Tipo select
    * Opções do select: Leite, Doce, Iogurte

O botão SALVAR irá adicionar as informações do produto em um ContextAPI local que será de repositório dos produtos cadastrados.

Se adicionado com sucesso, limpar todos os dados do formulário.

Caso o código do SKU já estiver adicionado no repositório, exibir uma mensagem que o código do SKU já está cadastrado. Neste caso, não limpar os dados preenchidos, fazendo com que o usuário possa alterar os dados para adicionar novamente.

**Todos os campos são obrigatórios.**

Realize uma validação dos dados de acordo com os seus tipos e exiba um feedback com o campo com erro.


Tela de visualização da lista de produtos
-----------------------------------------

Exibir uma tabela com Filtro e Ordenação com as seguintes colunas:

* SKU
* Nome
* Preço
* Categoria
* Ações

Na coluna de ações, adicionar um botão com ícone de lixeira, que ao clicar, irá apagar aquele registro do produto.


Repositório de dados
--------------------

Criar um ContextAPI que irá englobar a tela para salvar o repositório de dados dos produtos cadastrados. Não é necessário fazer a persistência dos dados (em localStorage ou API), o dado pode ser apenas local e em memória dentro do próprio ContextAPI.

**IMPORTANTE: utilizar ContextAPI. Não utilizar Redux.**

-----


Bibliotecas para utilização
---------------------------

* **MaterialUI [Obrigatório]**
* react-data-table-component
* react-hook-form

-----


Pontos de avaliação [OBRIGATÓRIO]
---------------------------------

- [ ] Typescript
- [ ] Rotas com divisão pública e privada
- [ ] Tela de login
- [ ] Sidebar na parte privada
- [ ] Tela de cadastro
- [ ] Validação de SKU duplicado
- [ ] Validação dos campos (tipo e mandatório) com feedbacks de erros
- [ ] Tela de visualização
- [ ] Tabela com visualização dos dados, ordenação e filtros
- [ ] Botão de remover registro
- [ ] Realizar commits bem descritos e bom gerenciamento do repositório GIT
- [ ] Boa organização de arquivos e diretórios
- [ ] Documentação


Pontos de avaliação [Opcional/Extra/Diferencial]
------------------------------

- [ ] TopBar com abertura/fechamento da sidebar
- [ ] Modal de confirmação de remoção de registro
- [ ] Persistência dos dados dos produtos cadastrados no localStorage (não perder ao recarregar a página)
- [ ] Criação de novos usuários e buscar login de um repositório local
- [ ] Edição de dados do registro na tabela de visualização
- [ ] Utilização da biblioteca react-data-table-component
- [ ] Utilização da biblioteca react-hook-form
- [ ] Utilização de bibliotecas que simulam APIs (ou desenvolver uma API) para consumo e persistência dos dados
- [ ] Testes de unidade

